;;lisp tic tac toe iteration 1.
;;student: Eduardo Flores Bautista
;;---------------------------------------------------------------------------------------------------


;;---------------------------------------------------------------------------------------------------
;; QUICK COPY PASTE FUNCTION CALLS

;;(Print-board *sampleboard*)

;;(Print-board *new-board*)

;;(load "~/code/lisptictactoe/ttt.lisp")

;;(victory (grab-col *new-board* 1))

;;(grab-col (*new-board* 1))

;;(play *new-board*)
;;---------------------------------------------------------------------------------------------------


;;---------------------------------------------------------------------------------------------------
;;GLOBAL VARIABLES
;;---------------------------------------------------------------------------------------------------
;; Example board - represented as a list of nine markers on the
;; tic-tac-toe board
(defparameter *sampleboard* (list 'x 'x 'x '- 'o '- '- 'o '-))

;;a board representing a new game.
(defparameter *new-board* (list '- '- '- '- '- '- '- '- '-))

(defparameter *player-name* (list 'e 'd 'd 'y ))

(defvar *bot-name* "cpu")

(defvar *x* "0");;global variable for x

(defvar *y* "0");;global variable for y

(defvar *bot-counter* 0)


;;---------------------------------------------------------------------------------------------------


;;---------------------------------------------------------------------------------------------------
;;TIC TAC TOE FUNCTIONS
;;---------------------------------------------------------------------------------------------------

;; Prints a board that is a list of nine symbols in row-major order
(defun print-board (board)
  (format t "=============")
  (do ( (i 0 (+ i 1)))
      ( (= i 9) 'done)
      ;; all these expressions are revaled in order every iteration
      (if ( = (mod i 3) 0)  (format t "~%|") nil)
      (format t " ~A |" (nth i board))
  )
  (format t "~%=============")
)

;; Determines if three items are all equal to each other
(defun threequal (a b c)
  (and (equal a b) (equal b c)))

;; Determines if a list representing a row or column or diagonal
;; of a tic-tac-toe board is a victory
;; FIXME: board prints nil if there is --- and (xxx or ooo).
;;(defun victory (alist)
;;  (if (not(eql (first alist) '-))
;;      (if (not(eql (second alist) '-))
;;          (if (not(eql (third alist) '-))
;;               (and (equal (first alist) (second alist))
;;               (equal (second alist) (third alist)))
;;          )
;;       )
;;  )
;;)
(defun check-victory()
  (or (threequal 0 1 2) ;top across
    (threequal 0 3 6) ;left down
    (threequal 0 4 8) ;diagonal
    (threequal 1 6 7) ;down middle
    (threequal 2 5 8) ;down right
    (threequal 2 6 8) ;diagonal
    (threequal 4 6 7) ;across middle
    (threequal 6 7 8) ;across bottom
  )
)



;; Returns a list consisting of the nth row (zero-based) of
;; a tic-tac-toe board
(defun grab-row (board row)
  (let ((x (* 3 row)))
    (list (nth x board)
          (nth (+ x 1) board)
          (nth (+ x 2) board))
  )
)

;; Returns a list consisting of the nth column (zero-based) of
;; a tic-tac-toe board
(defun grab-col (board col)
  (list (nth col board)
        (nth (+ col 3) board)
        (nth (+ col 6) board)))

;;---------------------------------------------------------------------------------------------------
;;ITERATION 1 FUNCTIONS
;;---------------------------------------------------------------------------------------------------
;;VICTORY FUNCTION NEEDS FURTHER DEVELOPMENT.

;;make a move
;;adds an x based on the x y location given by the user.
;;Enhancement: adds an x to a location on the board that contains an -.
(defun make-move (board x y)
    ;;start by multiplying x*y to get the correct location in the list to change.
    ;;this is stored in N.
    (let ((N (* x y)))
      (setf (nth N board) X);;enter an x in the nth spot on board.
    )
)

;;bot move
;;for now will place an o to the nearest open position.
;;Enhancement: make bot make a move that will link the most o's.
;;(defun bot-move (board)
;;   (if(eql(car board)('-));;if the car contains an -
;;         ;;(let ((N 1))   (setf (nth N board) B) )
;;         (setcar board 1)
;;         (bot-move (cdr board));;else just call bot-move with the cdr.
;;    )
;;
;;---------------------------------------------------------------------------------------------------
;;ITERATION 2 FUNCTIONS
;;---------------------------------------------------------------------------------------------------
;;GAME LOOP
;;
;;prints the player name.
(defun print-name (board)
  (format t "it's ")
  (do ( (i 0 (+ i 1)))
      ( (= i 4) 'done)
      ;; all these expressions are revealed in order every iteration
      (format t " ~A" (nth i board))
  )
  (format t "'s turn")
  )
;;
;;

;;loop to simulate playing a game of tic tac toe with a bot.
;;make a move function will tell user when to use the make a move function.
(defun play(board)
    (do ( (i 0 (+ i 1)))
        ( (= i 9) 'done)
        ;; all these expressions are revaled in order every iteration
        (format t " ~A" (nth i board))
        (print-board board)
        (if(= (mod i 2) 1);;if odd than its the players turn.
            (make-move board (ask-for-x) (ask-for-y))
            ;;(write *bot-name*)
            (bot-move board)
        )
    )
)

;;ask player function asks for an x input
(defun ask-for-x ()
    (write *player-name*)
    (write "enter x")
    (setq *x* (read))
)

;;ask player function asks for an y input
(defun ask-for-y ()
    (write "enter y")
    (setq *y* (read))
)
;;---------------------------------------------------------------------------------------------------
;;ITERATION 3 FUNCTIONS
;;---------------------------------------------------------------------------------------------------
;;fixed all above functions and impliment a check for VICTORY
;;in the game loop.
;;
;;
;;will make an move on the board.
;;on every move the program will place the letter "o" on the board.
;;it will check all 9 places in the list to see if..
;;___the place contains a -
;;___if so it will place in three lists, bad, good, win.
;;___first check for a win move, than good than a bad move.
;;___win moves connect three  o's
;;___good move connects two o's

(defun bot-move(board)
  ;;9 times( 9 elements check the char of the list and pass cdr)
  (if (null (cdr board));;if we are on the 10th element than print board.
    (print board)
    (if (string= "-" (car board))
      (bot-place-o board *bot-counter*)
      (setq *bot-counter* (+ *bot-counter* 1))
    )
  )
  (bot-move(cdr board))
)
;;infinite loop in above function.
;;
;;
;;places a 'o' on the board at place 'number'
(defun bot-place-o (board number)
    ;;start by multiplying x*y to get the correct location in the list to change.
    ;;this is stored in N.

      (setf (nth number board) 'o);;enter an x in the nth spot on board.
)
